#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<mpi.h> 
#define ITER 100
#define BLOCK 2

void init (int length,int *ca )
{
	int i,j;
	srand(time(NULL));
	for(i=0;i<length;i++)
	{
		for(j=0;j<length;j++)
		{
			ca[i*length+j]=rand()%2; 
		}
	}
}

void outputs (int length,int *ca )
{
	int i,j;
	for(i=0;i<length;i++)
	{
		for(j=0;j<length;j++)
		{
			
				printf("%3d ",ca[i*length+j]);
		}
		printf("\n");
	
	}
}


void critters_k(int myid,int procs,int length,int *ca,MPI_Status status )//自动更新 
{
	int i,j,t;
	int rotates;
for(t=0;t<ITER;t++)
	{	

	for(i=1;i<length-1;i++)
	{
		
		for(j=1;j<length-1;j++)//
		{
			int endi=(i+BLOCK)>length-1?(length-1):(i+BLOCK);
			int endj=(j+BLOCK)>length-1?(length-1):(j+BLOCK);
			for(int ii=i;ii<endi;ii++){
				for(int jj=j;jj<endj;jj++){
					
					if(length-myid+ii*procs>jj){//right
						rotates =ca[(ii-1)*length+jj-1]+ca[(ii-1)*length+jj]+ca[(ii-1)*length+jj+1]
						+ca[ii*length+jj-1] +ca[ii*length+jj+1]
						+ca[(ii+1)*length+jj-1]+ca[(ii+1)*length+jj]+ca[(ii+1)*length+jj+1];
						/*for(int p=a;p<procs;p++){
						MPI_Send(&ca[ii*length+jj],1, MPI_INT,p,110, MPI_COMM_WORLD);
					
						MPI_Recv(&ca[ii*length+jj],1, MPI_INT,p,110, MPI_COMM_WORLD, &status);
						}*/
					}
					else{//left or y=n-x
						rotates =ca[(ii-1)*length+jj-1]+ca[(ii-1)*length+jj]+ca[(ii-1)*length+jj+1]
						+ca[ii*length+jj-1] +ca[ii*length+jj+1]
						+ca[(ii+1)*length+jj-1]+ca[(ii+1)*length+jj]+ca[(ii+1)*length+jj+1];
					}
				//MPI_Gather(&rotates, 1, MPI_INT,&rotates, 1, MPI_INT,0, MPI_COMM_WORLD);
					if(rotates ==0 || rotates ==4 || rotates ==1)
						ca[ii*length+jj]=1;
					else if(rotates ==2)
						ca[ii*length+jj]=0;
					else
						ca[ii*length+jj]=ca[(length-1-ii)*length+(length-1-jj)];
			}
			}
				
		}
	}
	}
	
}		

int main(int argc, char** argv)
{
	int length =20;
    length=atoi(argv[1]);
	int hw=length*length;
	int myid = 0, procs = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Status status;
	int *arotatesay=(int*)malloc(length*length*sizeof(int));
	if(myid==0){
		init(length,arotatesay);
	}
	
	MPI_Bcast(arotatesay,length*length,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&length,1,MPI_INT,0,MPI_COMM_WORLD);
	
	critters_k(myid,procs,length,arotatesay,status);
	if(myid==0){
		outputs(length,arotatesay);
	}
	
	free(arotatesay);

	 MPI_Finalize();
	//
	return 0;
}
